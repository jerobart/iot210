import subprocess

def StartBeacon():
    #On start, beacon the OOBE experience.
    #hciconfig hci0 up    
    #hciconfig hci0 leadv 3
    subprocess.call("sudo hciconfig hci0 up", shell=True)
    subprocess.call("sudo hciconfig hci0 leadv 3", shell=True)
    subprocess.call("sudo hcitool -i hci0 cmd 0x08 0x0008 1e 02 01 06 03 03 aa fe 16 16 aa fe 10 00 03 74 69 6e 79 75 72 6c 00 79 62 35 64 62 79 65 77 00", shell=True) 

if __name__ == '__main__':
    StartBeacon()

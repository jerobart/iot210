#!/bin/bash
#https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/


#systemctl stop hostapd
#systemctl stop dnsmasq
cp /home/pi/Documents/git/iot210/Capstone/oobe/dhcpcd.conf.default /etc/dhcpcd.conf
cp /home/pi/Documents/git/iot210/Capstone/oobe/dnsmasq.conf.default /etc/dnsmasq.conf
cp /home/pi/Documents/git/iot210/Capstone/oobe/hostapd.config.default /etc/default/hostapd
cp /home/pi/Documents/git/iot210/Capstone/oobe/interfaces.default /etc/network/interfaces
rm /etc/supervisor/conf.d/startbeacon.conf
rm /etc/supervisor/conf.d/wificonfig.conf
rm /etc/nginx/sites-enabled/oobe-nginx.conf

rm /home/pi/Documents/git/iot210/Capstone/runservice/*.pem
rm /home/pi/Documents/git/iot210/Capstone/runservice/thing.json
cp /home/pi/Documents/git/iot210/Capstone/runservice/runservice.conf /etc/supervisor/conf.d/

reboot

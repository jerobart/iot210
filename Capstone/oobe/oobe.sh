#!/bin/bash
#Configure the pi to provide a private wifi network.
#Provide DHCP addresses on it.
#Just enough wifi to bootstrap our daily wifi config


systemctl stop hostapd
systemctl stop dnsmasq

#Configure a static IP address on wlan0
cp dhcpcd.conf.accesspoint /etc/dhcpcd.conf


#Configure DHCP address range to hand out
cp dnsmasq.conf.accesspoint /etc/dnsmasq.conf


#Configure private Wireless network
cp hostapd.conf.accesspoint /etc/hostapd/hostapd.conf
cp hostapd.config.accesspoint /etc/default/hostapd

cp interfaces.accesspoint /etc/network/interfaces

cp btlbeacon/startbeacon.conf /etc/supervisor/conf.d/

#Prepare to configure wireless acess point
cp oobe-nginx.conf /etc/nginx/sites-enabled
cp webapp/wificonfig.conf  /etc/supervisor/conf.d/



#!/bin/bash
#https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/

cp /etc/dhcpcd.conf ~/Documents/git/iot210/Capstone/accesspoint/dhcpcd.conf.default
cp /etc/dnsmasq.conf ~/Documents/git/iot210/Capstone/accesspoint/dnsmasq.conf.default
cp /etc/hostapd/hostapd.conf ~/Documents/git/iot210/Capstone/accesspoint/hostapd.conf.default
cp /etc/default/hostapd ~/Documents/git/iot210/Capstone/accesspoint/hostapd.config.default
cp /etc/sysctl.conf ~/Documents/git/iot210/Capstone/accesspoint/sysctl.conf.default
cp /etc/iptables.ipv4.nat ~/Documents/git/iot210/Capstone/accesspoint/iptables.ipv4.nat.default
cp /etc/network/interfaces ~/Documents/git/iot210/Capstone/accesspoint/interfaces.default 





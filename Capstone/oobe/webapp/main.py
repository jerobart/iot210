from flask import Flask, render_template, redirect, url_for, request
import subprocess
import logging
from logging.handlers import RotatingFileHandler


app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['wifiname'] == '':
            error = 'Wifiname cannot be blank.  Please try again.'
        else:
            writeWifiConfig(request.form['wifiname'],request.form['password'])
            subprocess.check_call("sudo /home/pi/Documents/git/iot210/Capstone/oobe/go.sh", shell=True)
            return redirect(url_for('healthcheck'))
    return render_template('wifipassword.html', error=error)


@app.route('/healthcheck')
def healthcheck():
    return "The OOBE Experience is up."

def writeWifiConfig(network, password):
    with open("/etc/wpa_supplicant/wpa_supplicant.conf", 'w') as f:
        f.write("ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n")
        f.write("update_config=1\n")
        f.write('network={\n')
        f.write('\tssid="' + network + '"\n')
        f.write('\tpsk="' + password + '"\n')
        f.write('\tkey_mgmt=WPA-PSK\n')
        f.write('}\n')

if __name__ == '__main__':
    handler = RotatingFileHandler('/etc/RobartsCapstoneOOBE.log', maxBytes=100000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.logger.info('Info')
    app.run()

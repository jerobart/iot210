My capstone project is to work through bringing a device from out of the box to being 
registered in our AWS IOT service.

We start the capstone by running the oobe.sh script:

cd oobe
sudo ./oobe.sh
sudo halt

A card in the box says to open the application's app and follow the link.
For our purposes we  open PhyWeb to get a beacon that will give you further instructions.

The beacon takes you to http://robartscapstone.simplesite.com.

"Connect to your IOT Device!!!!
To get started, connect to the WIFI network ROBARTS-CAPSTONE with password 123456789.
 
The open the following site in Chrome:
https://192.168.0.10/
 
This will help you connect to the wifi network.
It will then reboot the Pi.
When booted, the Pi will then display "Robarts Capstone Thing" on the display.
After that will be the identifier for your thing.
Write that down for AWS purposes.
If you miss it, reboot.
 
Each name is a guid.  It is displayed in parts multiple times, separated by a -
For example, for cad9076d-d993-4686-a771-574bec3b99b7
cad9076d is displayed 4 times, followed by a -.
d993 is displayed 4 times, followed by a -.
Continue with each guid part.
The final - is not part of the guid.
 
 
You can then connect to the device's MQTT topic for the sensehat:
Server: data.iot.us-west-2.amazonaws.com:8883
Topic: $aws/things//PiSensea"


On boot, the device is configured to have its own WIFI network, DHCP server,
NGINX server and Flask app.  The user then connects to the Flask App  (oobe/webapp)
which gets the user's wifi network and password.

On Login button press, the Flask app writes out the WPA configuration, copies the network configuration to take down the 
dhcp server, private network, static ip, nginx, andn flask app.  It then directs supervisord
to start the runservice.py script on boot.

runservice.py then checks to see if we've already downloaded an identity from AWS.  If not,
it calls the AWS lambda from Lab 6, downloading an identity for the device and certificates for 
authentication.  If these are present, it just loads the cached value.

runservice then displays the identity of the device on the sensehat's display.  Given guids are hard,
the guid is broken into its parts and each part is displayed multiple times.  When the identity display is 
finished, the service loop starts, collecting data from the sense hat and sending it to AWS via the device's
MQTT topic.

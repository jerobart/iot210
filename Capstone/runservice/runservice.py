#! /bin/python

import paho.mqtt.client as paho
import os
import os.path
import socket
import ssl
import sys
import requests
import json
from sense import PiSenseHat
import time


def on_connect(client, userdata, flags, rc):
  global connflag
  connflag = True
  print("Connection returned result: " + str(rc) )

def on_message(client, userdata, msg):
  print(msg.topic+" "+str(msg.payload))



caPath      = "awsIotRootCert"
awshost     = "data.iot.us-west-2.amazonaws.com"
awsport     = 8883
thingPath   = "/home/pi/Documents/git/iot210/Capstone/runservice/thing.json"
certPath    = "/home/pi/Documents/git/iot210/Capstone/runservice/cert.pem"
publicKeyPath    = "/home/pi/Documents/git/iot210/Capstone/runservice/publicKey.pem"
keyPath     = "/home/pi/Documents/git/iot210/Capstone/runservice/privatekey.pem"

#Do we have a device identity?

if not (os.path.exists(certPath) and os.path.exists(publicKeyPath) and os.path.exists(keyPath) and os.path.exists(thingPath)):
    headers = { "x-api-key" : "DftJWRhNy05HLRVJZ7pY65gliuFWVfJg8bIxQ192",
                "Content-Type" : "application/json" }

    data = {"Name": "Kevin Bacon", "Purpose": "Being Footloose","Description": "Awesome","Picture": "You Wish","Location": "Earth","DegreesOfSeparationFromKevinBacon": "0"}

    r = requests.post('https://qujuybe4la.execute-api.us-west-2.amazonaws.com/Lab6/AddRobot', headers=headers, data=json.dumps(data))

    thing = r.json()

    with open (certPath, "w") as f:
        f.write(thing["certificatePem"])

    with open (publicKeyPath, "w") as f:
        f.write(thing["keyPair"]["PublicKey"])

    with open (keyPath, "w") as f:
        f.write(thing["keyPair"]["PrivateKey"])

    with open (thingPath, "w") as f:
        f.write(json.dumps(thing))
else:
    with open(thingPath) as f:
        thing = json.load(f)

print(thing["thingName"])

topic_name = "$aws/things/%s/PiSense" % thing["thingName"]

print(topic_name)

client = paho.Client()
client.on_connect = on_connect
client.on_message = on_message

client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect(awshost, awsport, keepalive=60)

pi_sense_hat = PiSenseHat()

pi_sense_hat.show_message("Robarts Capstone Thing:")
time.sleep(0.1)

parts = thing["thingName"].split("-")

for part in parts:
    pi_sense_hat.show_message(part)
    time.sleep(0.1)
    pi_sense_hat.show_message(part)
    time.sleep(0.1)
    pi_sense_hat.show_message(part)
    time.sleep(0.1)
    pi_sense_hat.show_message(part)
    time.sleep(0.1)
    pi_sense_hat.show_message("-")

while True:
  msg = json.dumps(pi_sense_hat.getAllSensors())              
  client.publish(topic_name, msg)  
  client.loop()
  time.sleep(0.5)
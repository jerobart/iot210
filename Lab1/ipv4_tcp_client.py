#!/usr/bin/python
# -----------------------------------------------------------------------------
#        File : ipv4_tcp_client.py
# Description : TCP client using sockets
#      Author : Drew Gislsason
#        Date : 3/8/2017
# -----------------------------------------------------------------------------
import socket
import sys
import time

# signon
print "\nipv4_tcp_client message [-n #] [ip_addr [port]]\n"
if len(sys.argv) < 2:
  print 'Need a message to send. Try: python ipv4_tcp_client.py "hello world"'
  exit(1)

# message is first argument
message = sys.argv[1]
n_times = 1

# optional -n # (send message n times)
nextArg = 2
if len(sys.argv) > nextArg + 1 and sys.argv[nextArg] == "-n":
  n_times = int(sys.argv[nextArg + 1])
  nextArg = 4

# port
if len(sys.argv) > nextArg:
  PORT = int(sys.argv[nextArg])
else:
  PORT = 3333
nextArg += 1

# IP address
if len(sys.argv) > nextArg:
  HOST_IP = sys.argv[nextArg]
else:
  HOST_IP = "127.0.0.1"   # IPv4 localhost

print "Target Host IP:", HOST_IP
print "Target Port:", PORT
print 'Sending "' + str(message) + '"',
if n_times > 1:
  print str(n_times) + " times",
print "..."

sock = socket.socket(socket.AF_INET,      # Internet
                     socket.SOCK_STREAM)  # UDP
sock.connect((HOST_IP, PORT))

for i in xrange(0,n_times):
  sock.send(message)
  time.sleep(1)

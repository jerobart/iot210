#!/usr/bin/python
# =============================================================================
#        File : ipv4_tcp_server.py
# Description : Displays whatever comes in on the TCP connection
#      Author : Drew Gislsason
#        Date : 3/8/2017
# =============================================================================
import socket
import sys

MAX_CONNECTIONS   = 12

# ipv4_tcp_server [port [ip_addr]]
print "\nipv4_tcp_server message [port [ip_addr]]\n"

# optional port
if len(sys.argv) > 1:
  PORT = int(sys.argv[1])
else:
  PORT = 3333

# optional IP address
if len(sys.argv) > 2:
  HOST_IP = sys.argv[2]
else:
  HOST_IP = ''

print "Listening on IPv4 TCP port ", str(PORT)

# create TCP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# bind socket to port
server_addr = (HOST_IP, PORT)
sock.bind(server_addr)

# listen on the socket
sock.listen(MAX_CONNECTIONS)

while True:
  # wait for a connection
  print 'waiting for connection...'
  connection, client_addr = sock.accept()
  exit_now = False

  try:
    print 'connection from', client_addr

    # receive the data
    while True:
      data = connection.recv(2048)
      if data:
        print "received (" + str(len(data)) + ") bytes data: " + data
        if data == "exit()":
          exit_now = True
      else:
        break

  finally:

    # clean up
    print 'closing connection'
    connection.close()
    if exit_now:
      print "remote exit..."
      break

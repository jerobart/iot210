from sense_hat import SenseHat

sense = SenseHat()

for i in range(1,10,1):
    t = sense.get_temperature()
    p = sense.get_pressure()
    h = sense.get_humidity()
    t = round(t, 1)
    p = round(p, 1)
    h = round(h, 1)
    msg = "Temperature = %s, Pressure=%s, Humidity=%s" % (t,p,h)
    #sense.show_message(msg, scroll_speed=0.05)
    print(msg)


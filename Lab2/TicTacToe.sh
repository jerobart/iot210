#!/bin/bash
clear

set -o verbose

curl http://127.0.0.1:5000/tictactoe/admin/

curl -X POST http://127.0.0.1:5000/tictactoe/game/

curl -X POST http://127.0.0.1:5000/tictactoe/game/

curl -X POST http://127.0.0.1:5000/tictactoe/game/

curl http://127.0.0.1:5000/tictactoe/admin/

curl http://127.0.0.1:5000/tictactoe/game/?id=1

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=0&y=0&player=X"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=0&y=1&player=O"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=0&y=2&player=X"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=1&y=0&player=O"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=1&y=1&player=X"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=1&y=2&player=O"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=2&y=0&player=X"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=2&y=1&player=O"

curl -X PUT "http://127.0.0.1:5000/tictactoe/game/?id=1&x=2&y=2&player=X"

curl http://127.0.0.1:5000/tictactoe/admin/

curl -X DELETE http://127.0.0.1:5000/tictactoe/admin/

curl http://127.0.0.1:5000/tictactoe/admin/

curl http://127.0.0.1:5000/tictactoe/isalive/
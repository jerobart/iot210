#!/usr/bin/python
# =============================================================================
#        File : TicTacToeFlask.py
# Description : Simple TicTacToe server
#      Author : Jason Robarts
#        Date : 1/18/2018
# =============================================================================
import random
import string
import json
import sys

PORT = 5000

from flask import Flask, request

# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

board = "_________"
currentGame = 0
games = dict()

def makePretty(gameboard):
   return gameboard[:3] + "\n" + gameboard[3:6] + "\n" + gameboard[6:] + "\n"

@app.route("/tictactoe/game/", methods=['GET', 'POST', 'PUT', 'DELETE'])
def game():
  global games
  global currentGame
  global board
  rsp_data = ""
  
  if request.method == 'GET':
    id = int(request.args.get('id'))
    rsp_data += makePretty(games[id])
  elif request.method == 'PUT':
    id = int(request.args.get('id'))
    x = int(request.args.get('x'))
    y = int(request.args.get('y'))
    player = request.args.get('player')
    pos =  y * 3 + x
    games[id] = games[id][:pos] + str(player) + games[id][pos + 1:]
    rsp_data += makePretty(games[id])
  elif request.method == 'POST':
    currentGame = currentGame + 1
    games[currentGame] = board
    rsp_data += str(currentGame)
  elif request.method == 'DELETE':
    id = int(request.args.get('id'))
    del games[id]
    
  rsp_data += "\n"
    
  return rsp_data, 200

@app.route("/tictactoe/admin/", methods=['GET', 'DELETE'])
def admin():
  global games
  global currentGame
  global board
  rsp_data = ""

  if request.method == 'GET':
    rsp_data += "currentGame: " + str(currentGame) + " DictSize: " + str(len(games)) + "\n"
  elif request.method == 'DELETE':
    games = dict()
    rsp_data += "currentGame: " + str(currentGame) + " DictSize: " + str(len(games)) + "\n"

  return rsp_data, 200

#Simple routine for outside-in monitoring
@app.route("/tictactoe/isalive/", methods=['GET'])
def health():
  rsp_data = ""
  return rsp_data, 200


# ============================== Main ====================================

if __name__ == "__main__":

  print "TicTacToe"

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)


# protoc --python_out=. simple-jason.proto
# creates file simple-jason_pb2.py
import sys
import simple_jason_pb2

foo = simple_jason_pb2.simple()

foo.myInt32 = 2096;
foo.myString = "TestString";
foo.myFixed = 2096;
foo.myBool = True;
foo.myBytes = 'TestString';
foo.myInt64 = 2096;

with open("simple-jason.bin", "wb") as f:
  f.write(foo.SerializeToString())
  print "created 'simple-jason.bin'\n"

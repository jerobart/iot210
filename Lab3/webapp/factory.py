from sense import SimpleSenseHat
from jsonsense import JsonSenseHatDecorator
from xmlsense import XmlSenseHatDecorator
from protobuf import ProtoBufSenseHatDecorator

class sensorDataFactory:

    def __init__(self):
        self.simple = SimpleSenseHat()
        self.decorators = [JsonSenseHatDecorator(self.simple),
                           XmlSenseHatDecorator(self.simple),
                           ProtoBufSenseHatDecorator(self.simple)]
        self.default = JsonSenseHatDecorator(self.simple)

    
    def GetDecorator(self, contentType):
        for decorator in self.decorators:
            if contentType == decorator.getContentType():    
                return decorator
            
        return self.default

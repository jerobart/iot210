#!/usr/bin/python

from sense import SimpleSenseHat
import json

class JsonSenseHatDecorator:

    def __init__(self, simplesensehat):
        self.simple = simplesensehat
        self.contentType = "application/json"


    def getSensors(self):
        sensor = self.simple.getSensors()
        return json.dumps(sensor, indent=4, sort_keys=True)

    def getContentType(self):
        return self.contentType


def main():
    hat = SimpleSenseHat()
    decoratorHat = JsonSenseHatDecorator(hat)

    print(decoratorHat.getSensors())
    print(decoratorHat.getContentType())

if __name__=="__main__":
    main()








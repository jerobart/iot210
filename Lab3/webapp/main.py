#!/usr/bin/python
import time
from factory import sensorDataFactory
from flask import *

decoratorFactory = sensorDataFactory()

# ============================== API Routes ===================================
app = Flask(__name__)

# =========================== Endpoint: /myData ===============================
# read the sensor values by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/sensehat')
def sensehat():
    decorator = decoratorFactory.GetDecorator(request.headers['Content-Type'])
    return Response(decorator.getSensors(), mimetype=decorator.getContentType())
    
# ============================== API Routes ===================================

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)

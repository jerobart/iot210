#!/usr/bin/python

from sense import SimpleSenseHat
import sensors_pb2
import base64
import json

# {
#     "humidity": {
#         "unit": "%RH",
#         "value": 24.105770111083984
#     },
#     "meas_time": "2018-02-20T02:56:03.182251",
#     "name": "sense-hat",
#     "pressure": {
#         "unit": "mbar",
#         "value": 1015.908935546875
#     },
#     "temperature": {
#         "unit": "C",
#         "value": 34.404701232910156
#     }
# }

class ProtoBufSenseHatDecorator:
    def __init__(self, simplesensehat):
        self.simple = simplesensehat
        self.contentType = "application/protobuf"


    def getSensors(self):
        sensor = self.simple.getSensors()

        sensehat = sensors_pb2.sensors()
        sensehat.meas_time = str(sensor['meas_time'])
        sensehat.percentRelativeHumidity = sensor["humidity"]['value']
        sensehat.pressureInMbar = sensor["pressure"]['value']
        sensehat.temperatureC = sensor["temperature"]['value'] 

        return base64.b64encode(sensehat.SerializeToString())

    def getContentType(self):
        return self.contentType


def main():
    hat = SimpleSenseHat()
    decoratorHat = ProtoBufSenseHatDecorator(hat)

    b64 = decoratorHat.getSensors()
    pbuf = sensors_pb2.sensors()
    pbuf.ParseFromString(base64.b64decode(b64))

    print(b64)
    print(pbuf.SerializeToString())
    print(decoratorHat.getContentType())

if __name__=="__main__":
    main()

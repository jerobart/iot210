#!/usr/bin/python
#Modfied sensehat class from iot110 Lab8

from sense_hat import SenseHat
from datetime import datetime
import pprint

class SimpleSenseHat(object):
 
    # Constructor
    def __init__(self):
        self.sense = SenseHat()
        # enable all IMU functions
        self.sense.set_imu_config(True, True, True)

    def getSensors(self):
        sensors = {'name' : 'sense-hat'}
        sensors['pressure'] = { 'value':self.sense.get_pressure(), 'unit':'mbar'}
        sensors['temperature'] = { 'value':self.sense.get_temperature(), 'unit':'C'}
        sensors['humidity'] = { 'value':self.sense.get_humidity(), 'unit': '%RH'}
        sensors['meas_time'] = datetime.now().isoformat()
        return sensors


def main():

    # create an instance of my pi sense-hat sensor object
    pi_sense_hat = SimpleSenseHat()

    sensors =pi_sense_hat.getSensors()

    print("================== Dictionary Object =====================")
    pprint.pprint(pi_sense_hat.getSensors   ())
    print("==========================================================\n")

    # Read Parameters.
    p = sensors['pressure']
    t_c = sensors['temperature']
    h = sensors['humidity']
    
    print("================ Discrete Sensor Values ==================")
    print "      Pressure :", p
    print "   Temperature :", t_c
    print "      Humidity :", h
    print("==========================================================\n")
    

if __name__=="__main__":
   main()

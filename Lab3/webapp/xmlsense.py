#!/usr/bin/python
from sense import SimpleSenseHat
import xml.etree.ElementTree as ET

# {
#     "humidity": {
#         "unit": "%RH",
#         "value": 24.105770111083984
#     },
#     "meas_time": "2018-02-20T02:56:03.182251",
#     "name": "sense-hat",
#     "pressure": {
#         "unit": "mbar",
#         "value": 1015.908935546875
#     },
#     "temperature": {
#         "unit": "C",
#         "value": 34.404701232910156
#     }
# }

class XmlSenseHatDecorator:

    def __init__(self, simplesensehat):
        self.simple = simplesensehat
        self.contentType = "application/xml"


    def getSensors(self):
        sensor = self.simple.getSensors()

        sensehat = ET.Element('sensehat')
        sensehat.attrib['meas_time'] = str(sensor['meas_time'])

        humidity = ET.SubElement(sensehat, 'humidity')
        humidity.attrib['unit'] = sensor["humidity"]["unit"]
        humidity.text = str(sensor["humidity"]['value'])

        pressure = ET.SubElement(sensehat, 'pressure')
        pressure.attrib['unit'] = sensor["pressure"]["unit"]
        pressure.text = str(sensor["pressure"]['value'])

        temperature = ET.SubElement(sensehat, 'temperature')
        temperature.attrib['unit'] = sensor["temperature"]["unit"]
        temperature.text = str(sensor["temperature"]['value'])

        return ET.tostring(sensehat)

    def getContentType(self):
        return self.contentType


def main():
    hat = SimpleSenseHat()
    decoratorHat = XmlSenseHatDecorator(hat)

    print(decoratorHat.getSensors())
    print(decoratorHat.getContentType())

if __name__=="__main__":
    main()

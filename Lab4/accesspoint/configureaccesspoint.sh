#https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/


systemctl stop hostapd
systemctl stop dnsmasq
cp dhcpcd.conf.accesspoint /etc/dhcpcd.conf
cp dnsmasq.conf.accesspoint /etc/dnsmasq.conf
cp hostapd.conf.accesspoint /etc/hostapd/hostapd.conf
cp hostapd.config.accesspoint /etc/default/hostapd
cp sysctl.conf.accesspoint /etc/sysctl.conf
cp iptables.ipv4.nat.accesspoint /etc/iptables.ipv4.nat
brctl addbr br0
brctl addif br0 eth0
cp interfaces.accesspoint /etc/network/interfaces




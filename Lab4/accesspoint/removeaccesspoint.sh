#https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/


systemctl stop hostapd
systemctl stop dnsmasq
cp dhcpcd.conf.default /etc/dhcpcd.conf
cp dnsmasq.conf.default /etc/dnsmasq.conf
cp hostapd.conf.default /etc/hostapd/hostapd.conf
cp hostapd.config.default /etc/default/hostapd
cp sysctl.conf.default /etc/sysctl.conf
cp iptables.ipv4.nat.default /etc/iptables.ipv4.nat
brctl delif br0 eth0
cp interfaces.default /etc/network/interfaces




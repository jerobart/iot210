#!/usr/bin/python
# =============================================================================
#        File : dropbox_sha256.py
# Description : allows creating a password database on dropbox
#      Author : Drew Gislsason
#        Date : 4/6/2017
# =============================================================================
# built-in libraries
import base64
import getpass
import sys
import string
import os

# installed libraries
from Crypto.Hash import SHA256
import dropbox

# See: http://md5decrypt.net/en/Sha256/
# //www.dropbox.com/developers/apps/info/appkey

TOKEN    = 'gfSbK_iPNrwAAAAAAAAACf29Z8xBChXFI6MT-4NII0-clgFa0TBfc-YAZptat9qG'
FILENAME = '/passwords.txt'
SALT     = '' # '$n3@_'
INITIAL  = 'hello,486ea46224d1bb4fb680f34f7c9ad96a8f24ec88be73ea8e5a6c65260e9cb8a7\n'

DEBUG    = 0

STATUS_OK         = 0
STATUS_FAILED     = 1
STATUS_DUPLICATE  = 2

TMPFILE = "x.txt"

# login using secure token
def dropbox_login(token):
  dbx = dropbox.Dropbox(token)
  try:
    info = dbx.users_get_current_account()
    return dbx
  except:
    print "not a valid token"
    return None

# get the whole file from dropbox into memory as a string
def dropbox_getfile(dbx, filename):
  try:
    os.remove(TMPFILE)
  except:
    print "no tmp file"
  try:
    dbx.files_download_to_file(TMPFILE,filename)
    f = open(TMPFILE)
    content = f.read()
    f.close()
    return content
  except:
    print "could not download file"
    return None

# write a dropbox file, given entire file contents.
# Returns size of file, or 0 if failed
def dropbox_putfile(dbx, filename, filecontents):
  try:
    dbx.files_delete(filename)
  except:
    print "no file to delete"
  try:
    dbx.files_alpha_upload(filecontents, filename)
    return len(filecontents)
  except:
    print "failed to upload"
    return 0

# if username matches, return password sha256
def iot_find_password_sha(uname, filecontents):

  # empty file or user, not found
  if uname == None or filecontents == None:
    return None

  # look through all the lines
  sha = None
  lines = filecontents.split('\n')
  for line in lines:
    if DEBUG: print "line " + str(line)
    i = string.find(line, ',')
    if i > 0 and uname == line[0:i]:
      sha = line[i+1:]
  return sha

# create a user with hashed password
# returns status,filecontents
def iot_create_account(filecontents):

  # don't allow creating the same name twice
  uname = raw_input("Enter username to create: ")
  if iot_find_password_sha(uname, filecontents):
    return STATUS_DUPLICATE, filecontents

  # get username and password in one line
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  line  = uname + ',' + sha1 + '\n'

  # append it to the file
  filecontents = filecontents + line
  if DEBUG: print filecontents
  return STATUS_OK, filecontents

# login a user with hashed password
def iot_login_account(uname, filecontents):

  if DEBUG: print "uname " + str(uname)
  if DEBUG: print "file  " + str(filecontents)

  # get sha from password
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  if DEBUG: print "sha1: " + str(sha1)

  # login if the sha matches
  sha2  = iot_find_password_sha(uname, filecontents)
  if DEBUG: print "sha2: " + str(sha2)
  if sha2 == sha1:
    return True
  return False

# main function
def main():
  print "dropbox_sha256 v1.0"

  # login to dropbox
  dbx = dropbox_login(TOKEN)
  if dbx:
    print "\nlogged into dropbox"
    filecontents = dropbox_getfile(dbx, FILENAME)
    if not filecontents:
      filecontents = INITIAL
      dropbox_putfile(dbx, FILENAME, filecontents)
  else:
    print "\nfailed to log in to dropbox!"
    exit(1)

  while True:
    # get username
    uname = raw_input("\nEnter name to login (leave empty for new user): ")
    if uname == 'exit':
      break

    # create an account
    elif uname == '':
      status, filecontents = iot_create_account(filecontents)
      if status == STATUS_OK:
        if dropbox_putfile(dbx, FILENAME, filecontents) == 0:
          print "Something went wrong"
        else:
          print "Account created"
      elif status == STATUS_DUPLICATE:
        print "Account already exists"

    # login to an account
    else:
      if iot_login_account(uname, filecontents):
        print "Welcome " + uname + "! Logged in"
      else:
        print "Login failed!!"

if __name__ == "__main__":
  if len(TOKEN) <= len("{token}"):
    print "\nLooks like you didn't add your token. Go generate one for"
    print "your app API at: https://www.dropbox.com/developers/apps\n"
  main()

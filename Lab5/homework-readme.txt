In homework_lab5.py we see that we're able to connect to the dropbox account and enumerate the contents.
Following that a file is uploaded.
The homework_lab5.transcript shows the output, that a previous upload of "cavs vs warriors" succeeded,
and that the upload did not fail.
#!/usr/bin/python
# =============================================================================
#        File : hsl2hue.py
# Description : Converts HSL colors from degrees, saturation %, level %
#      Author : Drew Gislsason
#        Date : 5/16/2017
# =============================================================================
import sys

def hsl2hue(hue,sat,bri):
  if hue >= 360: hue = 0
  if sat > 100:  sat = 100
  if bri > 100:  bri = 100

  hue = hue * 65536 / 360
  sat = sat * 254 / 100
  bri = bri * 254 / 100

  return hue,sat,bri

if __name__ == "__main__":

  if len(sys.argv) < 4:
    print "\n  python hsl2hue.py degrees saturation_percent level_percent"
    print "\nexample: hsl2hue.py 271 68 32"
    exit(1)

  hue, sat, bri = hsl2hue(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))
  print '{"on":true,' + '"hue":{0},"sat":{1},"bri":{2}'.format(hue,sat,bri) + '}'

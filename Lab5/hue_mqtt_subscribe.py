#!/usr/bin/python
# =============================================================================
#        File : mqtt_subscribe.py
# Description : Subscribe to data from an MQTT Broker
#      Author : Drew Gislsason
#        Date : 4/7/2017
# =============================================================================
# pip install paho-mqtt
import os
import sys
import paho.mqtt.client as mqtt

# command will go over MQTT in form of:  HueCmd(Name): {"on":true}
PREFIX      = "HueCmd("
AFTERNAME   = "): "
topic_name = "iot210hue"

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    print "Subscribing to " + topic_name
    client.subscribe(topic_name)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    s = str(msg.payload)
    n = s.find(AFTERNAME)
    if n >= len(PREFIX) and s[0:len(PREFIX)] == PREFIX:
      name = s[len(PREFIX):n]
      data = s[n+len(AFTERNAME):]
      print(msg.topic + "from (" + name + ") data: '" + str(data) + "'")
    else:
      print "bad mqtt msg '" + str(s) + "'"

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("iot.eclipse.org", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()

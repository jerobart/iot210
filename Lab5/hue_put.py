#!/usr/bin/python
# =============================================================================
#        File : hue_put.py
# Description : Allows controlling a Philips Hue light (Light 2)
#      Author : Drew Gislsason
#        Date : 5/16/2017
# =============================================================================
import httplib
import random
import sys
import time
import os

# [Get Hue IP Addr](https://www.meethue.com/api/nupnp)  
# [Philips Hue Login](https://developers.meethue.com/user/login)  
# [Philips Hue Getting Started](https://developers.meethue.com/documentation/getting-started)  
# [Philips Hue Lights API](https://developers.meethue.com/documentation/lights-api)  
# [Some Colors in HSL](http://www.december.com/html/spec/colorhsl.html)  
# [Wikipedia Description of HSL](https://en.wikipedia.org/wiki/HSL_and_HSV)  
# [Wikipedia Description of CIE](https://en.wikipedia.org/wiki/CIE_1931_color_space)  

# Manual Control via Browser  {ipAddr}/debug/clip.html

# Example: 
BRIDGE_IP   = '192.168.1.128'  # '172.22.194.nnn' at UW
LIGHT_ID    = '2'
BRIDGE_API_GET = '/api/SZgzJfdaZcDzjqvmSqob-KL0o1WrDDpUAMblSEBc/lights/' + LIGHT_ID
BRIDGE_API_PUT = '/api/SZgzJfdaZcDzjqvmSqob-KL0o1WrDDpUAMblSEBc/lights/' + LIGHT_ID + '/state'

if len(sys.argv) >= 2 and sys.argv[1] == '--help':
  print "\npython hue_put.py cmd"
  print "example: hue_put.py '{\"on\":true, \"hue\":0, \"sat\":0, \"bri\":254}'\n"
  exit(1)

fGet = True
data = ''
if len(sys.argv) >= 2:
  fGet = False
  data = sys.argv[1]

# contact philips hue
conn = httplib.HTTPConnection(BRIDGE_IP, 80)
if fGet:
  conn.request('GET', BRIDGE_API_GET)
else:
  conn.request('PUT', BRIDGE_API_PUT, data )

# get the response
r1 = conn.getresponse()
print "status " + str(r1.status) + ", reason " + str(r1.reason)
data1 = r1.read()
print "return data: " + str(data1)

conn.close()

from __future__ import print_function # Python 2/3 compatibility
import boto3

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')


table = dynamodb.create_table(
    TableName='IOT210-Robots-01',
    KeySchema=[
        {
            'AttributeName': 'partitionKey',
            'KeyType': 'HASH'  #Partition key
        },
        {
            'AttributeName': 'name',
            'KeyType': 'RANGE'  #Sort key
        }
    ],
    AttributeDefinitions=[        
        
            {
                'AttributeName': 'name',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'purpose',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'description',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'picture',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'location',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'degreesofseparationfromkevinbacon',
                'AttributeType': 'N'
            }
        
    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
)

print("Table status:", table.table_status)

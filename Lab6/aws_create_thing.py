#!/usr/bin/python
# =============================================================================
#        File : aws_iot_creatething.py
# Description : Create a thing on AWS IOT
#      Author : Jason Robarts
#        Date : 3/8/2018
# =============================================================================
import boto3
import sys

#Lamba uses the role provided on create for identity.
#So we just need the basic code to create the thing,
#the same way as we would on-prem from the Pi

client = boto3.client('iot')

Name = "NewThing-JasonRobarts"

response = client.create_thing(
    thingName=Name    
)

print response


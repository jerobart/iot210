#!/usr/bin/python
# =============================================================================
#        File : awsiotpub.py
# Description : Publish to iot210 topic
#      Author : Drew Gislsason
#        Date : 5/25/2017
# =============================================================================
import paho.mqtt.client as paho
import os
import socket
import ssl
import sys

topic_name = "iot210"

def on_connect(client, userdata, flags, rc):
  global connflag
  connflag = True
  print("Connection returned result: " + str(rc) )

def on_message(client, userdata, msg):
  print(msg.topic+" "+str(msg.payload))

client = paho.Client()
client.on_connect = on_connect
client.on_message = on_message

awshost     = "data.iot.us-west-2.amazonaws.com"
awsport     = 8883
thingName   = "thing"
caPath      = "awsIotRootCert"
certPath    = "cert.pem"
keyPath     = "privkey.pem"

client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect(awshost, awsport, keepalive=60)
client.loop_start()

while True:
  if sys.argv > 1:
    topic_name = raw_input("Enter Topic Name: ")
  s = raw_input("Enter a string to publish: ")
  client.publish(topic_name, s)

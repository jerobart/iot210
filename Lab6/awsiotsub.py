#!/usr/bin/python
# =============================================================================
#        File : awsiotsub.py
# Description : Subscribes to all topics 
#      Author : Drew Gislsason
#        Date : 5/25/2017
# =============================================================================
import paho.mqtt.client as paho
import os
import socket
import ssl

def on_connect(client, userdata, flags, rc):
  print("Connection returned result: " + str(rc) )
  client.subscribe("#" , 1 )

def on_message(client, userdata, msg):
  print("topic: "+msg.topic)
  print("payload: "+str(msg.payload))

client = paho.Client()
client.on_connect = on_connect
client.on_message = on_message

awshost     = "data.iot.us-west-2.amazonaws.com"
awsport     = 8883
clientId    = "iotbutton_jasonrobarts"
thingName   = "thing"
caPath      = "awsIotRootCert"
certPath    = "cert.pem"
keyPath     = "privkey.pem"

print "Subscribing to all topics on AWS"

client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect(awshost, awsport, keepalive=60)
client.loop_forever()

/*=============================================================================
        File : udp_client.c
 Description : UDP client using sockets
      Author : Drew Gislsason
        Date : 2/22/2018
=============================================================================*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SRV_IP  "127.0.0.1"
#define PORT    3333
void die(char *s)
{
    printf("Error: %s\n",s);
    exit(1);
}

int main(int argc, char *argv[])
{
    struct sockaddr_in  si_other;
    unsigned            port = PORT;
    char *              s_ip = SRV_IP;
    int                 sock, i;
    socklen_t           slen=sizeof(si_other);

    printf("UDP Server in C for IoT 210\n");
    printf("udp_server message [port [ip_addr]]\n\n");

    if(argc < 2)
        die("Too few arguments");

    if(argc > 2)
        port = atoi(argv[2]);
    if(argc > 3)
        s_ip = argv[3];
    printf("Sending '%s' to port %u, IP Addr %s\n",argv[1],port,s_ip);


    if ((sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
        die("can't open socket");

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port);
    if (inet_aton(s_ip, (void *)&si_other.sin_addr)==0)
        die("inet_aton() failed");

    if(sendto(sock, argv[1], strlen(argv[1]), 0, (void *)&si_other, slen)==-1)
        die("sendto()");

    close(sock);
    return 0;
}
